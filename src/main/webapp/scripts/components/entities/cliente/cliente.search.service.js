'use strict';

angular.module('autocenterMwmApp')
    .factory('ClienteSearch', function ($resource) {
        return $resource('api/_search/clientes/:query', {}, {
            'query': { method: 'GET', isArray: true}
        });
    });

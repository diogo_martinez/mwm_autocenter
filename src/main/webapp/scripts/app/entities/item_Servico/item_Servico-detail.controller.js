'use strict';

angular.module('autocenterMwmApp')
    .controller('Item_ServicoDetailController', function ($scope, $rootScope, $stateParams, entity, Item_Servico, Pedido) {
        $scope.item_Servico = entity;
        $scope.load = function (id) {
            Item_Servico.get({id: id}, function(result) {
                $scope.item_Servico = result;
            });
        };
        var unsubscribe = $rootScope.$on('autocenterMwmApp:item_ServicoUpdate', function(event, result) {
            $scope.item_Servico = result;
        });
        $scope.$on('$destroy', unsubscribe);

    });

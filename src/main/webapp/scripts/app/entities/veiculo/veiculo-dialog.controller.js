'use strict';

angular.module('autocenterMwmApp').controller('VeiculoDialogController',
    ['$scope', '$stateParams', '$uibModalInstance', 'entity', 'Veiculo', 'Cliente', 'Pedido',
        function($scope, $stateParams, $uibModalInstance, entity, Veiculo, Cliente, Pedido) {

        $scope.veiculo = entity;
        $scope.clientes = Cliente.query();
        $scope.pedidos = Pedido.query();
        $scope.load = function(id) {
            Veiculo.get({id : id}, function(result) {
                $scope.veiculo = result;
            });
        };

        var onSaveSuccess = function (result) {
            $scope.$emit('autocenterMwmApp:veiculoUpdate', result);
            $uibModalInstance.close(result);
            $scope.isSaving = false;
        };

        var onSaveError = function (result) {
            $scope.isSaving = false;
        };

        $scope.save = function () {
            $scope.isSaving = true;
            if ($scope.veiculo.id != null) {
                Veiculo.update($scope.veiculo, onSaveSuccess, onSaveError);
            } else {
                Veiculo.save($scope.veiculo, onSaveSuccess, onSaveError);
            }
        };

        $scope.clear = function() {
            $uibModalInstance.dismiss('cancel');
        };
}]);

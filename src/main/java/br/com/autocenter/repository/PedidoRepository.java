package br.com.autocenter.repository;

import br.com.autocenter.domain.Pedido;

import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Spring Data JPA repository for the Pedido entity.
 */
public interface PedidoRepository extends JpaRepository<Pedido,Long> {

    @Query("select distinct pedido from Pedido pedido left join fetch pedido.item_Servicos")
    List<Pedido> findAllWithEagerRelationships();

    @Query("select pedido from Pedido pedido left join fetch pedido.item_Servicos where pedido.id =:id")
    Pedido findOneWithEagerRelationships(@Param("id") Long id);

}

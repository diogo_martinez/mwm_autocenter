package br.com.autocenter.web.rest;

import br.com.autocenter.Application;
import br.com.autocenter.domain.Pedido;
import br.com.autocenter.repository.PedidoRepository;
import br.com.autocenter.repository.search.PedidoSearchRepository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.time.LocalDate;
import java.time.ZoneId;
import java.math.BigDecimal;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


/**
 * Test class for the PedidoResource REST controller.
 *
 * @see PedidoResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@IntegrationTest
public class PedidoResourceIntTest {


    private static final LocalDate DEFAULT_DATA_PEDIDO = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_PEDIDO = LocalDate.now(ZoneId.systemDefault());

    private static final Integer DEFAULT_KILOMETRAGEM = 1;
    private static final Integer UPDATED_KILOMETRAGEM = 2;
    private static final String DEFAULT_OBSERVACOES = "AAAAA";
    private static final String UPDATED_OBSERVACOES = "BBBBB";
    private static final String DEFAULT_FUNCIONARIO = "AAAAA";
    private static final String UPDATED_FUNCIONARIO = "BBBBB";

    private static final BigDecimal DEFAULT_TOTAL = new BigDecimal(1);
    private static final BigDecimal UPDATED_TOTAL = new BigDecimal(2);

    @Inject
    private PedidoRepository pedidoRepository;

    @Inject
    private PedidoSearchRepository pedidoSearchRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    private MockMvc restPedidoMockMvc;

    private Pedido pedido;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        PedidoResource pedidoResource = new PedidoResource();
        ReflectionTestUtils.setField(pedidoResource, "pedidoSearchRepository", pedidoSearchRepository);
        ReflectionTestUtils.setField(pedidoResource, "pedidoRepository", pedidoRepository);
        this.restPedidoMockMvc = MockMvcBuilders.standaloneSetup(pedidoResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void initTest() {
        pedido = new Pedido();
        pedido.setData_pedido(DEFAULT_DATA_PEDIDO);
        pedido.setKilometragem(DEFAULT_KILOMETRAGEM);
        pedido.setObservacoes(DEFAULT_OBSERVACOES);
        pedido.setFuncionario(DEFAULT_FUNCIONARIO);
        pedido.setTotal(DEFAULT_TOTAL);
    }

    @Test
    @Transactional
    public void createPedido() throws Exception {
        int databaseSizeBeforeCreate = pedidoRepository.findAll().size();

        // Create the Pedido

        restPedidoMockMvc.perform(post("/api/pedidos")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(pedido)))
                .andExpect(status().isCreated());

        // Validate the Pedido in the database
        List<Pedido> pedidos = pedidoRepository.findAll();
        assertThat(pedidos).hasSize(databaseSizeBeforeCreate + 1);
        Pedido testPedido = pedidos.get(pedidos.size() - 1);
        assertThat(testPedido.getData_pedido()).isEqualTo(DEFAULT_DATA_PEDIDO);
        assertThat(testPedido.getKilometragem()).isEqualTo(DEFAULT_KILOMETRAGEM);
        assertThat(testPedido.getObservacoes()).isEqualTo(DEFAULT_OBSERVACOES);
        assertThat(testPedido.getFuncionario()).isEqualTo(DEFAULT_FUNCIONARIO);
        assertThat(testPedido.getTotal()).isEqualTo(DEFAULT_TOTAL);
    }

    @Test
    @Transactional
    public void checkData_pedidoIsRequired() throws Exception {
        int databaseSizeBeforeTest = pedidoRepository.findAll().size();
        // set the field null
        pedido.setData_pedido(null);

        // Create the Pedido, which fails.

        restPedidoMockMvc.perform(post("/api/pedidos")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(pedido)))
                .andExpect(status().isBadRequest());

        List<Pedido> pedidos = pedidoRepository.findAll();
        assertThat(pedidos).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkKilometragemIsRequired() throws Exception {
        int databaseSizeBeforeTest = pedidoRepository.findAll().size();
        // set the field null
        pedido.setKilometragem(null);

        // Create the Pedido, which fails.

        restPedidoMockMvc.perform(post("/api/pedidos")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(pedido)))
                .andExpect(status().isBadRequest());

        List<Pedido> pedidos = pedidoRepository.findAll();
        assertThat(pedidos).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkFuncionarioIsRequired() throws Exception {
        int databaseSizeBeforeTest = pedidoRepository.findAll().size();
        // set the field null
        pedido.setFuncionario(null);

        // Create the Pedido, which fails.

        restPedidoMockMvc.perform(post("/api/pedidos")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(pedido)))
                .andExpect(status().isBadRequest());

        List<Pedido> pedidos = pedidoRepository.findAll();
        assertThat(pedidos).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkTotalIsRequired() throws Exception {
        int databaseSizeBeforeTest = pedidoRepository.findAll().size();
        // set the field null
        pedido.setTotal(null);

        // Create the Pedido, which fails.

        restPedidoMockMvc.perform(post("/api/pedidos")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(pedido)))
                .andExpect(status().isBadRequest());

        List<Pedido> pedidos = pedidoRepository.findAll();
        assertThat(pedidos).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllPedidos() throws Exception {
        // Initialize the database
        pedidoRepository.saveAndFlush(pedido);

        // Get all the pedidos
        restPedidoMockMvc.perform(get("/api/pedidos?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(pedido.getId().intValue())))
                .andExpect(jsonPath("$.[*].data_pedido").value(hasItem(DEFAULT_DATA_PEDIDO.toString())))
                .andExpect(jsonPath("$.[*].kilometragem").value(hasItem(DEFAULT_KILOMETRAGEM)))
                .andExpect(jsonPath("$.[*].observacoes").value(hasItem(DEFAULT_OBSERVACOES.toString())))
                .andExpect(jsonPath("$.[*].funcionario").value(hasItem(DEFAULT_FUNCIONARIO.toString())))
                .andExpect(jsonPath("$.[*].total").value(hasItem(DEFAULT_TOTAL.intValue())));
    }

    @Test
    @Transactional
    public void getPedido() throws Exception {
        // Initialize the database
        pedidoRepository.saveAndFlush(pedido);

        // Get the pedido
        restPedidoMockMvc.perform(get("/api/pedidos/{id}", pedido.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(pedido.getId().intValue()))
            .andExpect(jsonPath("$.data_pedido").value(DEFAULT_DATA_PEDIDO.toString()))
            .andExpect(jsonPath("$.kilometragem").value(DEFAULT_KILOMETRAGEM))
            .andExpect(jsonPath("$.observacoes").value(DEFAULT_OBSERVACOES.toString()))
            .andExpect(jsonPath("$.funcionario").value(DEFAULT_FUNCIONARIO.toString()))
            .andExpect(jsonPath("$.total").value(DEFAULT_TOTAL.intValue()));
    }

    @Test
    @Transactional
    public void getNonExistingPedido() throws Exception {
        // Get the pedido
        restPedidoMockMvc.perform(get("/api/pedidos/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updatePedido() throws Exception {
        // Initialize the database
        pedidoRepository.saveAndFlush(pedido);

		int databaseSizeBeforeUpdate = pedidoRepository.findAll().size();

        // Update the pedido
        pedido.setData_pedido(UPDATED_DATA_PEDIDO);
        pedido.setKilometragem(UPDATED_KILOMETRAGEM);
        pedido.setObservacoes(UPDATED_OBSERVACOES);
        pedido.setFuncionario(UPDATED_FUNCIONARIO);
        pedido.setTotal(UPDATED_TOTAL);

        restPedidoMockMvc.perform(put("/api/pedidos")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(pedido)))
                .andExpect(status().isOk());

        // Validate the Pedido in the database
        List<Pedido> pedidos = pedidoRepository.findAll();
        assertThat(pedidos).hasSize(databaseSizeBeforeUpdate);
        Pedido testPedido = pedidos.get(pedidos.size() - 1);
        assertThat(testPedido.getData_pedido()).isEqualTo(UPDATED_DATA_PEDIDO);
        assertThat(testPedido.getKilometragem()).isEqualTo(UPDATED_KILOMETRAGEM);
        assertThat(testPedido.getObservacoes()).isEqualTo(UPDATED_OBSERVACOES);
        assertThat(testPedido.getFuncionario()).isEqualTo(UPDATED_FUNCIONARIO);
        assertThat(testPedido.getTotal()).isEqualTo(UPDATED_TOTAL);
    }

    @Test
    @Transactional
    public void deletePedido() throws Exception {
        // Initialize the database
        pedidoRepository.saveAndFlush(pedido);

		int databaseSizeBeforeDelete = pedidoRepository.findAll().size();

        // Get the pedido
        restPedidoMockMvc.perform(delete("/api/pedidos/{id}", pedido.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<Pedido> pedidos = pedidoRepository.findAll();
        assertThat(pedidos).hasSize(databaseSizeBeforeDelete - 1);
    }
}

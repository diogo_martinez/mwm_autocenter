'use strict';

angular.module('autocenterMwmApp')
	.controller('Item_ServicoDeleteController', function($scope, $uibModalInstance, entity, Item_Servico) {

        $scope.item_Servico = entity;
        $scope.clear = function() {
            $uibModalInstance.dismiss('cancel');
        };
        $scope.confirmDelete = function (id) {
            Item_Servico.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        };

    });

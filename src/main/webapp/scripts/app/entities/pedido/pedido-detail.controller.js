'use strict';

angular.module('autocenterMwmApp')
    .controller('PedidoDetailController', function ($scope, $rootScope, $stateParams, entity, Pedido, Veiculo, Item_Servico) {
        $scope.pedido = entity;
        $scope.load = function (id) {
            Pedido.get({id: id}, function(result) {
                $scope.pedido = result;
            });
        };
        var unsubscribe = $rootScope.$on('autocenterMwmApp:pedidoUpdate', function(event, result) {
            $scope.pedido = result;
        });
        $scope.$on('$destroy', unsubscribe);

    });

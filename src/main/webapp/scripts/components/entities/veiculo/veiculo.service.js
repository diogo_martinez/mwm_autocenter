'use strict';

angular.module('autocenterMwmApp')
    .factory('Veiculo', function ($resource, DateUtils) {
        return $resource('api/veiculos/:id', {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    data = angular.fromJson(data);
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    });

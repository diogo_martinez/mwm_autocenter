package br.com.autocenter.config;

import br.com.autocenter.aop.logging.LoggingAspect;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.*;

@Configuration
@EnableAspectJAutoProxy
public class LoggingAspectConfiguration {

	private final Logger log = LoggerFactory.getLogger(LoggingAspectConfiguration.class);
    @Bean
    @Profile(Constants.SPRING_PROFILE_DEVELOPMENT)
    public LoggingAspect loggingAspect() {
    	log.debug("starting loggingAspect");
        return new LoggingAspect();
    }
}

package br.com.autocenter.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A Item_Servico.
 */
@Entity
@Table(name = "item_servico")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "item_servico")
public class Item_Servico implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotNull
    @Column(name = "descricao", nullable = false)
    private String descricao;
    
    @NotNull
    @Column(name = "periodo_manutencao", nullable = false)
    private Integer periodo_manutencao;
    
    @Column(name = "preco_unitario", precision=10, scale=2)
    private BigDecimal preco_unitario;
    
    @ManyToMany(mappedBy = "item_Servicos")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Pedido> pedidos = new HashSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescricao() {
        return descricao;
    }
    
    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Integer getPeriodo_manutencao() {
        return periodo_manutencao;
    }
    
    public void setPeriodo_manutencao(Integer periodo_manutencao) {
        this.periodo_manutencao = periodo_manutencao;
    }

    public BigDecimal getPreco_unitario() {
        return preco_unitario;
    }
    
    public void setPreco_unitario(BigDecimal preco_unitario) {
        this.preco_unitario = preco_unitario;
    }

    public Set<Pedido> getPedidos() {
        return pedidos;
    }

    public void setPedidos(Set<Pedido> pedidos) {
        this.pedidos = pedidos;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Item_Servico item_Servico = (Item_Servico) o;
        if(item_Servico.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, item_Servico.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "Item_Servico{" +
            "id=" + id +
            ", descricao='" + descricao + "'" +
            ", periodo_manutencao='" + periodo_manutencao + "'" +
            ", preco_unitario='" + preco_unitario + "'" +
            '}';
    }
}

package br.com.autocenter.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import java.time.LocalDate;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A Pedido.
 */
@Entity
@Table(name = "pedido")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "pedido")
public class Pedido implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotNull
    @Column(name = "data_pedido", nullable = false)
    private LocalDate data_pedido;
    
    @NotNull
    @Column(name = "kilometragem", nullable = false)
    private Integer kilometragem;
    
    @Column(name = "observacoes")
    private String observacoes;
    
    @NotNull
    @Column(name = "funcionario", nullable = false)
    private String funcionario;
    
    @NotNull
    @Column(name = "total", precision=10, scale=2, nullable = false)
    private BigDecimal total;
    
    @ManyToOne
    @JoinColumn(name = "veiculo_id")
    private Veiculo veiculo;

    @ManyToMany
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @JoinTable(name = "pedido_item_servico",
               joinColumns = @JoinColumn(name="pedidos_id", referencedColumnName="ID"),
               inverseJoinColumns = @JoinColumn(name="item_servicos_id", referencedColumnName="ID"))
    private Set<Item_Servico> item_Servicos = new HashSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getData_pedido() {
        return data_pedido;
    }
    
    public void setData_pedido(LocalDate data_pedido) {
        this.data_pedido = data_pedido;
    }

    public Integer getKilometragem() {
        return kilometragem;
    }
    
    public void setKilometragem(Integer kilometragem) {
        this.kilometragem = kilometragem;
    }

    public String getObservacoes() {
        return observacoes;
    }
    
    public void setObservacoes(String observacoes) {
        this.observacoes = observacoes;
    }

    public String getFuncionario() {
        return funcionario;
    }
    
    public void setFuncionario(String funcionario) {
        this.funcionario = funcionario;
    }

    public BigDecimal getTotal() {
        return total;
    }
    
    public void setTotal(BigDecimal total) {
        this.total = total;
    }

    public Veiculo getVeiculo() {
        return veiculo;
    }

    public void setVeiculo(Veiculo veiculo) {
        this.veiculo = veiculo;
    }

    public Set<Item_Servico> getItem_Servicos() {
        return item_Servicos;
    }

    public void setItem_Servicos(Set<Item_Servico> item_Servicos) {
        this.item_Servicos = item_Servicos;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Pedido pedido = (Pedido) o;
        if(pedido.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, pedido.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "Pedido{" +
            "id=" + id +
            ", data_pedido='" + data_pedido + "'" +
            ", kilometragem='" + kilometragem + "'" +
            ", observacoes='" + observacoes + "'" +
            ", funcionario='" + funcionario + "'" +
            ", total='" + total + "'" +
            '}';
    }
}

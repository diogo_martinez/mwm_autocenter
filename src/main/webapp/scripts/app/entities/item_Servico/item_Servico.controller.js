'use strict';

angular.module('autocenterMwmApp')
    .controller('Item_ServicoController', function ($scope, $state, Item_Servico, Item_ServicoSearch, ParseLinks) {

        $scope.item_Servicos = [];
        $scope.predicate = 'id';
        $scope.reverse = true;
        $scope.page = 0;
        $scope.loadAll = function() {
            Item_Servico.query({page: $scope.page, size: 20, sort: [$scope.predicate + ',' + ($scope.reverse ? 'asc' : 'desc'), 'id']}, function(result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                for (var i = 0; i < result.length; i++) {
                    $scope.item_Servicos.push(result[i]);
                }
            });
        };
        $scope.reset = function() {
            $scope.page = 0;
            $scope.item_Servicos = [];
            $scope.loadAll();
        };
        $scope.loadPage = function(page) {
            $scope.page = page;
            $scope.loadAll();
        };
        $scope.loadAll();


        $scope.search = function () {
            Item_ServicoSearch.query({query: $scope.searchQuery}, function(result) {
                $scope.item_Servicos = result;
            }, function(response) {
                if(response.status === 404) {
                    $scope.loadAll();
                }
            });
        };

        $scope.refresh = function () {
            $scope.reset();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.item_Servico = {
                descricao: null,
                periodo_manutencao: null,
                preco_unitario: null,
                id: null
            };
        };
    });

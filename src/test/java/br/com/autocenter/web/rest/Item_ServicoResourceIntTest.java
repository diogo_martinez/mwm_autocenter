package br.com.autocenter.web.rest;

import br.com.autocenter.Application;
import br.com.autocenter.domain.Item_Servico;
import br.com.autocenter.repository.Item_ServicoRepository;
import br.com.autocenter.repository.search.Item_ServicoSearchRepository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.math.BigDecimal;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


/**
 * Test class for the Item_ServicoResource REST controller.
 *
 * @see Item_ServicoResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@IntegrationTest
public class Item_ServicoResourceIntTest {

    private static final String DEFAULT_DESCRICAO = "AAAAA";
    private static final String UPDATED_DESCRICAO = "BBBBB";

    private static final Integer DEFAULT_PERIODO_MANUTENCAO = 1;
    private static final Integer UPDATED_PERIODO_MANUTENCAO = 2;

    private static final BigDecimal DEFAULT_PRECO_UNITARIO = new BigDecimal(1);
    private static final BigDecimal UPDATED_PRECO_UNITARIO = new BigDecimal(2);

    @Inject
    private Item_ServicoRepository item_ServicoRepository;

    @Inject
    private Item_ServicoSearchRepository item_ServicoSearchRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    private MockMvc restItem_ServicoMockMvc;

    private Item_Servico item_Servico;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        Item_ServicoResource item_ServicoResource = new Item_ServicoResource();
        ReflectionTestUtils.setField(item_ServicoResource, "item_ServicoSearchRepository", item_ServicoSearchRepository);
        ReflectionTestUtils.setField(item_ServicoResource, "item_ServicoRepository", item_ServicoRepository);
        this.restItem_ServicoMockMvc = MockMvcBuilders.standaloneSetup(item_ServicoResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void initTest() {
        item_Servico = new Item_Servico();
        item_Servico.setDescricao(DEFAULT_DESCRICAO);
        item_Servico.setPeriodo_manutencao(DEFAULT_PERIODO_MANUTENCAO);
        item_Servico.setPreco_unitario(DEFAULT_PRECO_UNITARIO);
    }

    @Test
    @Transactional
    public void createItem_Servico() throws Exception {
        int databaseSizeBeforeCreate = item_ServicoRepository.findAll().size();

        // Create the Item_Servico

        restItem_ServicoMockMvc.perform(post("/api/item_Servicos")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(item_Servico)))
                .andExpect(status().isCreated());

        // Validate the Item_Servico in the database
        List<Item_Servico> item_Servicos = item_ServicoRepository.findAll();
        assertThat(item_Servicos).hasSize(databaseSizeBeforeCreate + 1);
        Item_Servico testItem_Servico = item_Servicos.get(item_Servicos.size() - 1);
        assertThat(testItem_Servico.getDescricao()).isEqualTo(DEFAULT_DESCRICAO);
        assertThat(testItem_Servico.getPeriodo_manutencao()).isEqualTo(DEFAULT_PERIODO_MANUTENCAO);
        assertThat(testItem_Servico.getPreco_unitario()).isEqualTo(DEFAULT_PRECO_UNITARIO);
    }

    @Test
    @Transactional
    public void checkDescricaoIsRequired() throws Exception {
        int databaseSizeBeforeTest = item_ServicoRepository.findAll().size();
        // set the field null
        item_Servico.setDescricao(null);

        // Create the Item_Servico, which fails.

        restItem_ServicoMockMvc.perform(post("/api/item_Servicos")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(item_Servico)))
                .andExpect(status().isBadRequest());

        List<Item_Servico> item_Servicos = item_ServicoRepository.findAll();
        assertThat(item_Servicos).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkPeriodo_manutencaoIsRequired() throws Exception {
        int databaseSizeBeforeTest = item_ServicoRepository.findAll().size();
        // set the field null
        item_Servico.setPeriodo_manutencao(null);

        // Create the Item_Servico, which fails.

        restItem_ServicoMockMvc.perform(post("/api/item_Servicos")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(item_Servico)))
                .andExpect(status().isBadRequest());

        List<Item_Servico> item_Servicos = item_ServicoRepository.findAll();
        assertThat(item_Servicos).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllItem_Servicos() throws Exception {
        // Initialize the database
        item_ServicoRepository.saveAndFlush(item_Servico);

        // Get all the item_Servicos
        restItem_ServicoMockMvc.perform(get("/api/item_Servicos?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(item_Servico.getId().intValue())))
                .andExpect(jsonPath("$.[*].descricao").value(hasItem(DEFAULT_DESCRICAO.toString())))
                .andExpect(jsonPath("$.[*].periodo_manutencao").value(hasItem(DEFAULT_PERIODO_MANUTENCAO)))
                .andExpect(jsonPath("$.[*].preco_unitario").value(hasItem(DEFAULT_PRECO_UNITARIO.intValue())));
    }

    @Test
    @Transactional
    public void getItem_Servico() throws Exception {
        // Initialize the database
        item_ServicoRepository.saveAndFlush(item_Servico);

        // Get the item_Servico
        restItem_ServicoMockMvc.perform(get("/api/item_Servicos/{id}", item_Servico.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(item_Servico.getId().intValue()))
            .andExpect(jsonPath("$.descricao").value(DEFAULT_DESCRICAO.toString()))
            .andExpect(jsonPath("$.periodo_manutencao").value(DEFAULT_PERIODO_MANUTENCAO))
            .andExpect(jsonPath("$.preco_unitario").value(DEFAULT_PRECO_UNITARIO.intValue()));
    }

    @Test
    @Transactional
    public void getNonExistingItem_Servico() throws Exception {
        // Get the item_Servico
        restItem_ServicoMockMvc.perform(get("/api/item_Servicos/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateItem_Servico() throws Exception {
        // Initialize the database
        item_ServicoRepository.saveAndFlush(item_Servico);

		int databaseSizeBeforeUpdate = item_ServicoRepository.findAll().size();

        // Update the item_Servico
        item_Servico.setDescricao(UPDATED_DESCRICAO);
        item_Servico.setPeriodo_manutencao(UPDATED_PERIODO_MANUTENCAO);
        item_Servico.setPreco_unitario(UPDATED_PRECO_UNITARIO);

        restItem_ServicoMockMvc.perform(put("/api/item_Servicos")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(item_Servico)))
                .andExpect(status().isOk());

        // Validate the Item_Servico in the database
        List<Item_Servico> item_Servicos = item_ServicoRepository.findAll();
        assertThat(item_Servicos).hasSize(databaseSizeBeforeUpdate);
        Item_Servico testItem_Servico = item_Servicos.get(item_Servicos.size() - 1);
        assertThat(testItem_Servico.getDescricao()).isEqualTo(UPDATED_DESCRICAO);
        assertThat(testItem_Servico.getPeriodo_manutencao()).isEqualTo(UPDATED_PERIODO_MANUTENCAO);
        assertThat(testItem_Servico.getPreco_unitario()).isEqualTo(UPDATED_PRECO_UNITARIO);
    }

    @Test
    @Transactional
    public void deleteItem_Servico() throws Exception {
        // Initialize the database
        item_ServicoRepository.saveAndFlush(item_Servico);

		int databaseSizeBeforeDelete = item_ServicoRepository.findAll().size();

        // Get the item_Servico
        restItem_ServicoMockMvc.perform(delete("/api/item_Servicos/{id}", item_Servico.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<Item_Servico> item_Servicos = item_ServicoRepository.findAll();
        assertThat(item_Servicos).hasSize(databaseSizeBeforeDelete - 1);
    }
}

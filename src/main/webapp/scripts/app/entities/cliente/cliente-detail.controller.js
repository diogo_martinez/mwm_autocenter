'use strict';

angular.module('autocenterMwmApp')
    .controller('ClienteDetailController', function ($scope, $rootScope, $stateParams, entity, Cliente, Veiculo) {
        $scope.cliente = entity;
        $scope.load = function (id) {
            Cliente.get({id: id}, function(result) {
                $scope.cliente = result;
            });
        };
        var unsubscribe = $rootScope.$on('autocenterMwmApp:clienteUpdate', function(event, result) {
            $scope.cliente = result;
        });
        $scope.$on('$destroy', unsubscribe);

    });

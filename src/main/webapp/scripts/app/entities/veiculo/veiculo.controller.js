'use strict';

angular.module('autocenterMwmApp')
    .controller('VeiculoController', function ($scope, $state, Veiculo, VeiculoSearch, ParseLinks) {

        $scope.veiculos = [];
        $scope.predicate = 'id';
        $scope.reverse = true;
        $scope.page = 0;
        $scope.loadAll = function() {
            Veiculo.query({page: $scope.page, size: 20, sort: [$scope.predicate + ',' + ($scope.reverse ? 'asc' : 'desc'), 'id']}, function(result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                for (var i = 0; i < result.length; i++) {
                    $scope.veiculos.push(result[i]);
                }
            });
        };
        $scope.reset = function() {
            $scope.page = 0;
            $scope.veiculos = [];
            $scope.loadAll();
        };
        $scope.loadPage = function(page) {
            $scope.page = page;
            $scope.loadAll();
        };
        $scope.loadAll();


        $scope.search = function () {
            VeiculoSearch.query({query: $scope.searchQuery}, function(result) {
                $scope.veiculos = result;
            }, function(response) {
                if(response.status === 404) {
                    $scope.loadAll();
                }
            });
        };

        $scope.refresh = function () {
            $scope.reset();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.veiculo = {
                placa: null,
                fabricante: null,
                modelo: null,
                ano_fabricacao: null,
                ano_modelo: null,
                cor: null,
                id: null
            };
        };
    });

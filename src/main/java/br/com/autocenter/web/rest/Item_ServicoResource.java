package br.com.autocenter.web.rest;

import com.codahale.metrics.annotation.Timed;
import br.com.autocenter.domain.Item_Servico;
import br.com.autocenter.repository.Item_ServicoRepository;
import br.com.autocenter.repository.search.Item_ServicoSearchRepository;
import br.com.autocenter.web.rest.util.HeaderUtil;
import br.com.autocenter.web.rest.util.PaginationUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing Item_Servico.
 */
@RestController
@RequestMapping("/api")
public class Item_ServicoResource {

    private final Logger log = LoggerFactory.getLogger(Item_ServicoResource.class);
        
    @Inject
    private Item_ServicoRepository item_ServicoRepository;
    
    @Inject
    private Item_ServicoSearchRepository item_ServicoSearchRepository;
    
    /**
     * POST  /item_Servicos -> Create a new item_Servico.
     */
    @RequestMapping(value = "/item_Servicos",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Item_Servico> createItem_Servico(@Valid @RequestBody Item_Servico item_Servico) throws URISyntaxException {
        log.debug("REST request to save Item_Servico : {}", item_Servico);
        if (item_Servico.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("item_Servico", "idexists", "A new item_Servico cannot already have an ID")).body(null);
        }
        Item_Servico result = item_ServicoRepository.save(item_Servico);
        item_ServicoSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/item_Servicos/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("item_Servico", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /item_Servicos -> Updates an existing item_Servico.
     */
    @RequestMapping(value = "/item_Servicos",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Item_Servico> updateItem_Servico(@Valid @RequestBody Item_Servico item_Servico) throws URISyntaxException {
        log.debug("REST request to update Item_Servico : {}", item_Servico);
        if (item_Servico.getId() == null) {
            return createItem_Servico(item_Servico);
        }
        Item_Servico result = item_ServicoRepository.save(item_Servico);
        item_ServicoSearchRepository.save(result);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("item_Servico", item_Servico.getId().toString()))
            .body(result);
    }

    /**
     * GET  /item_Servicos -> get all the item_Servicos.
     */
    @RequestMapping(value = "/item_Servicos",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<Item_Servico>> getAllItem_Servicos(Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to get a page of Item_Servicos");
        Page<Item_Servico> page = item_ServicoRepository.findAll(pageable); 
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/item_Servicos");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /item_Servicos/:id -> get the "id" item_Servico.
     */
    @RequestMapping(value = "/item_Servicos/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Item_Servico> getItem_Servico(@PathVariable Long id) {
        log.debug("REST request to get Item_Servico : {}", id);
        Item_Servico item_Servico = item_ServicoRepository.findOne(id);
        return Optional.ofNullable(item_Servico)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /item_Servicos/:id -> delete the "id" item_Servico.
     */
    @RequestMapping(value = "/item_Servicos/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteItem_Servico(@PathVariable Long id) {
        log.debug("REST request to delete Item_Servico : {}", id);
        item_ServicoRepository.delete(id);
        item_ServicoSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("item_Servico", id.toString())).build();
    }

    /**
     * SEARCH  /_search/item_Servicos/:query -> search for the item_Servico corresponding
     * to the query.
     */
    @RequestMapping(value = "/_search/item_Servicos/{query:.+}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<Item_Servico> searchItem_Servicos(@PathVariable String query) {
        log.debug("REST request to search Item_Servicos for query {}", query);
        return StreamSupport
            .stream(item_ServicoSearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .collect(Collectors.toList());
    }
}

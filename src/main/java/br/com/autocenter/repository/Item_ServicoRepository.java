package br.com.autocenter.repository;

import br.com.autocenter.domain.Item_Servico;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the Item_Servico entity.
 */
public interface Item_ServicoRepository extends JpaRepository<Item_Servico,Long> {

}

'use strict';

angular.module('autocenterMwmApp').controller('Item_ServicoDialogController',
    ['$scope', '$stateParams', '$uibModalInstance', 'entity', 'Item_Servico', 'Pedido',
        function($scope, $stateParams, $uibModalInstance, entity, Item_Servico, Pedido) {

        $scope.item_Servico = entity;
        $scope.pedidos = Pedido.query();
        $scope.load = function(id) {
            Item_Servico.get({id : id}, function(result) {
                $scope.item_Servico = result;
            });
        };

        var onSaveSuccess = function (result) {
            $scope.$emit('autocenterMwmApp:item_ServicoUpdate', result);
            $uibModalInstance.close(result);
            $scope.isSaving = false;
        };

        var onSaveError = function (result) {
            $scope.isSaving = false;
        };

        $scope.save = function () {
            $scope.isSaving = true;
            if ($scope.item_Servico.id != null) {
                Item_Servico.update($scope.item_Servico, onSaveSuccess, onSaveError);
            } else {
                Item_Servico.save($scope.item_Servico, onSaveSuccess, onSaveError);
            }
        };

        $scope.clear = function() {
            $uibModalInstance.dismiss('cancel');
        };
}]);

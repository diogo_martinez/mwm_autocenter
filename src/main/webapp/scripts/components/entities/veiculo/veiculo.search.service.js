'use strict';

angular.module('autocenterMwmApp')
    .factory('VeiculoSearch', function ($resource) {
        return $resource('api/_search/veiculos/:query', {}, {
            'query': { method: 'GET', isArray: true}
        });
    });

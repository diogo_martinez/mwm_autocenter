'use strict';

angular.module('autocenterMwmApp')
    .factory('Item_Servico', function ($resource, DateUtils) {
        return $resource('api/item_Servicos/:id', {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    data = angular.fromJson(data);
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    });

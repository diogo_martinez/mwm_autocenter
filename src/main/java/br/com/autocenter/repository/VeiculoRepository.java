package br.com.autocenter.repository;

import br.com.autocenter.domain.Veiculo;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the Veiculo entity.
 */
public interface VeiculoRepository extends JpaRepository<Veiculo,Long> {

}

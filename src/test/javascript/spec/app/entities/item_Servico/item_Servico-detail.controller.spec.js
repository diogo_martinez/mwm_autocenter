'use strict';

describe('Controller Tests', function() {

    describe('Item_Servico Detail Controller', function() {
        var $scope, $rootScope;
        var MockEntity, MockItem_Servico, MockPedido;
        var createController;

        beforeEach(inject(function($injector) {
            $rootScope = $injector.get('$rootScope');
            $scope = $rootScope.$new();
            MockEntity = jasmine.createSpy('MockEntity');
            MockItem_Servico = jasmine.createSpy('MockItem_Servico');
            MockPedido = jasmine.createSpy('MockPedido');
            

            var locals = {
                '$scope': $scope,
                '$rootScope': $rootScope,
                'entity': MockEntity ,
                'Item_Servico': MockItem_Servico,
                'Pedido': MockPedido
            };
            createController = function() {
                $injector.get('$controller')("Item_ServicoDetailController", locals);
            };
        }));


        describe('Root Scope Listening', function() {
            it('Unregisters root scope listener upon scope destruction', function() {
                var eventType = 'autocenterMwmApp:item_ServicoUpdate';

                createController();
                expect($rootScope.$$listenerCount[eventType]).toEqual(1);

                $scope.$destroy();
                expect($rootScope.$$listenerCount[eventType]).toBeUndefined();
            });
        });
    });

});

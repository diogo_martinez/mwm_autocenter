'use strict';

angular.module('autocenterMwmApp')
	.controller('VeiculoDeleteController', function($scope, $uibModalInstance, entity, Veiculo) {

        $scope.veiculo = entity;
        $scope.clear = function() {
            $uibModalInstance.dismiss('cancel');
        };
        $scope.confirmDelete = function (id) {
            Veiculo.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        };

    });

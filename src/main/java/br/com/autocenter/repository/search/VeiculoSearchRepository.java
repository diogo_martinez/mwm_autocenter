package br.com.autocenter.repository.search;

import br.com.autocenter.domain.Veiculo;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data ElasticSearch repository for the Veiculo entity.
 */
public interface VeiculoSearchRepository extends ElasticsearchRepository<Veiculo, Long> {
}

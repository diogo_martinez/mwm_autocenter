package br.com.autocenter.web.rest;

import br.com.autocenter.Application;
import br.com.autocenter.domain.Veiculo;
import br.com.autocenter.repository.VeiculoRepository;
import br.com.autocenter.repository.search.VeiculoSearchRepository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


/**
 * Test class for the VeiculoResource REST controller.
 *
 * @see VeiculoResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@IntegrationTest
public class VeiculoResourceIntTest {

    private static final String DEFAULT_PLACA = "AAAAA";
    private static final String UPDATED_PLACA = "BBBBB";
    private static final String DEFAULT_FABRICANTE = "AAAAA";
    private static final String UPDATED_FABRICANTE = "BBBBB";
    private static final String DEFAULT_MODELO = "AAAAA";
    private static final String UPDATED_MODELO = "BBBBB";

    private static final Integer DEFAULT_ANO_FABRICACAO = 1;
    private static final Integer UPDATED_ANO_FABRICACAO = 2;

    private static final Integer DEFAULT_ANO_MODELO = 1;
    private static final Integer UPDATED_ANO_MODELO = 2;
    private static final String DEFAULT_COR = "AAAAA";
    private static final String UPDATED_COR = "BBBBB";

    @Inject
    private VeiculoRepository veiculoRepository;

    @Inject
    private VeiculoSearchRepository veiculoSearchRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    private MockMvc restVeiculoMockMvc;

    private Veiculo veiculo;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        VeiculoResource veiculoResource = new VeiculoResource();
        ReflectionTestUtils.setField(veiculoResource, "veiculoSearchRepository", veiculoSearchRepository);
        ReflectionTestUtils.setField(veiculoResource, "veiculoRepository", veiculoRepository);
        this.restVeiculoMockMvc = MockMvcBuilders.standaloneSetup(veiculoResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void initTest() {
        veiculo = new Veiculo();
        veiculo.setPlaca(DEFAULT_PLACA);
        veiculo.setFabricante(DEFAULT_FABRICANTE);
        veiculo.setModelo(DEFAULT_MODELO);
        veiculo.setAno_fabricacao(DEFAULT_ANO_FABRICACAO);
        veiculo.setAno_modelo(DEFAULT_ANO_MODELO);
        veiculo.setCor(DEFAULT_COR);
    }

    @Test
    @Transactional
    public void createVeiculo() throws Exception {
        int databaseSizeBeforeCreate = veiculoRepository.findAll().size();

        // Create the Veiculo

        restVeiculoMockMvc.perform(post("/api/veiculos")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(veiculo)))
                .andExpect(status().isCreated());

        // Validate the Veiculo in the database
        List<Veiculo> veiculos = veiculoRepository.findAll();
        assertThat(veiculos).hasSize(databaseSizeBeforeCreate + 1);
        Veiculo testVeiculo = veiculos.get(veiculos.size() - 1);
        assertThat(testVeiculo.getPlaca()).isEqualTo(DEFAULT_PLACA);
        assertThat(testVeiculo.getFabricante()).isEqualTo(DEFAULT_FABRICANTE);
        assertThat(testVeiculo.getModelo()).isEqualTo(DEFAULT_MODELO);
        assertThat(testVeiculo.getAno_fabricacao()).isEqualTo(DEFAULT_ANO_FABRICACAO);
        assertThat(testVeiculo.getAno_modelo()).isEqualTo(DEFAULT_ANO_MODELO);
        assertThat(testVeiculo.getCor()).isEqualTo(DEFAULT_COR);
    }

    @Test
    @Transactional
    public void checkPlacaIsRequired() throws Exception {
        int databaseSizeBeforeTest = veiculoRepository.findAll().size();
        // set the field null
        veiculo.setPlaca(null);

        // Create the Veiculo, which fails.

        restVeiculoMockMvc.perform(post("/api/veiculos")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(veiculo)))
                .andExpect(status().isBadRequest());

        List<Veiculo> veiculos = veiculoRepository.findAll();
        assertThat(veiculos).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkFabricanteIsRequired() throws Exception {
        int databaseSizeBeforeTest = veiculoRepository.findAll().size();
        // set the field null
        veiculo.setFabricante(null);

        // Create the Veiculo, which fails.

        restVeiculoMockMvc.perform(post("/api/veiculos")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(veiculo)))
                .andExpect(status().isBadRequest());

        List<Veiculo> veiculos = veiculoRepository.findAll();
        assertThat(veiculos).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkModeloIsRequired() throws Exception {
        int databaseSizeBeforeTest = veiculoRepository.findAll().size();
        // set the field null
        veiculo.setModelo(null);

        // Create the Veiculo, which fails.

        restVeiculoMockMvc.perform(post("/api/veiculos")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(veiculo)))
                .andExpect(status().isBadRequest());

        List<Veiculo> veiculos = veiculoRepository.findAll();
        assertThat(veiculos).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkAno_fabricacaoIsRequired() throws Exception {
        int databaseSizeBeforeTest = veiculoRepository.findAll().size();
        // set the field null
        veiculo.setAno_fabricacao(null);

        // Create the Veiculo, which fails.

        restVeiculoMockMvc.perform(post("/api/veiculos")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(veiculo)))
                .andExpect(status().isBadRequest());

        List<Veiculo> veiculos = veiculoRepository.findAll();
        assertThat(veiculos).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkAno_modeloIsRequired() throws Exception {
        int databaseSizeBeforeTest = veiculoRepository.findAll().size();
        // set the field null
        veiculo.setAno_modelo(null);

        // Create the Veiculo, which fails.

        restVeiculoMockMvc.perform(post("/api/veiculos")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(veiculo)))
                .andExpect(status().isBadRequest());

        List<Veiculo> veiculos = veiculoRepository.findAll();
        assertThat(veiculos).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkCorIsRequired() throws Exception {
        int databaseSizeBeforeTest = veiculoRepository.findAll().size();
        // set the field null
        veiculo.setCor(null);

        // Create the Veiculo, which fails.

        restVeiculoMockMvc.perform(post("/api/veiculos")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(veiculo)))
                .andExpect(status().isBadRequest());

        List<Veiculo> veiculos = veiculoRepository.findAll();
        assertThat(veiculos).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllVeiculos() throws Exception {
        // Initialize the database
        veiculoRepository.saveAndFlush(veiculo);

        // Get all the veiculos
        restVeiculoMockMvc.perform(get("/api/veiculos?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(veiculo.getId().intValue())))
                .andExpect(jsonPath("$.[*].placa").value(hasItem(DEFAULT_PLACA.toString())))
                .andExpect(jsonPath("$.[*].fabricante").value(hasItem(DEFAULT_FABRICANTE.toString())))
                .andExpect(jsonPath("$.[*].modelo").value(hasItem(DEFAULT_MODELO.toString())))
                .andExpect(jsonPath("$.[*].ano_fabricacao").value(hasItem(DEFAULT_ANO_FABRICACAO)))
                .andExpect(jsonPath("$.[*].ano_modelo").value(hasItem(DEFAULT_ANO_MODELO)))
                .andExpect(jsonPath("$.[*].cor").value(hasItem(DEFAULT_COR.toString())));
    }

    @Test
    @Transactional
    public void getVeiculo() throws Exception {
        // Initialize the database
        veiculoRepository.saveAndFlush(veiculo);

        // Get the veiculo
        restVeiculoMockMvc.perform(get("/api/veiculos/{id}", veiculo.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(veiculo.getId().intValue()))
            .andExpect(jsonPath("$.placa").value(DEFAULT_PLACA.toString()))
            .andExpect(jsonPath("$.fabricante").value(DEFAULT_FABRICANTE.toString()))
            .andExpect(jsonPath("$.modelo").value(DEFAULT_MODELO.toString()))
            .andExpect(jsonPath("$.ano_fabricacao").value(DEFAULT_ANO_FABRICACAO))
            .andExpect(jsonPath("$.ano_modelo").value(DEFAULT_ANO_MODELO))
            .andExpect(jsonPath("$.cor").value(DEFAULT_COR.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingVeiculo() throws Exception {
        // Get the veiculo
        restVeiculoMockMvc.perform(get("/api/veiculos/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateVeiculo() throws Exception {
        // Initialize the database
        veiculoRepository.saveAndFlush(veiculo);

		int databaseSizeBeforeUpdate = veiculoRepository.findAll().size();

        // Update the veiculo
        veiculo.setPlaca(UPDATED_PLACA);
        veiculo.setFabricante(UPDATED_FABRICANTE);
        veiculo.setModelo(UPDATED_MODELO);
        veiculo.setAno_fabricacao(UPDATED_ANO_FABRICACAO);
        veiculo.setAno_modelo(UPDATED_ANO_MODELO);
        veiculo.setCor(UPDATED_COR);

        restVeiculoMockMvc.perform(put("/api/veiculos")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(veiculo)))
                .andExpect(status().isOk());

        // Validate the Veiculo in the database
        List<Veiculo> veiculos = veiculoRepository.findAll();
        assertThat(veiculos).hasSize(databaseSizeBeforeUpdate);
        Veiculo testVeiculo = veiculos.get(veiculos.size() - 1);
        assertThat(testVeiculo.getPlaca()).isEqualTo(UPDATED_PLACA);
        assertThat(testVeiculo.getFabricante()).isEqualTo(UPDATED_FABRICANTE);
        assertThat(testVeiculo.getModelo()).isEqualTo(UPDATED_MODELO);
        assertThat(testVeiculo.getAno_fabricacao()).isEqualTo(UPDATED_ANO_FABRICACAO);
        assertThat(testVeiculo.getAno_modelo()).isEqualTo(UPDATED_ANO_MODELO);
        assertThat(testVeiculo.getCor()).isEqualTo(UPDATED_COR);
    }

    @Test
    @Transactional
    public void deleteVeiculo() throws Exception {
        // Initialize the database
        veiculoRepository.saveAndFlush(veiculo);

		int databaseSizeBeforeDelete = veiculoRepository.findAll().size();

        // Get the veiculo
        restVeiculoMockMvc.perform(delete("/api/veiculos/{id}", veiculo.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<Veiculo> veiculos = veiculoRepository.findAll();
        assertThat(veiculos).hasSize(databaseSizeBeforeDelete - 1);
    }
}

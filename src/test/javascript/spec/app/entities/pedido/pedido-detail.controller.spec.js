'use strict';

describe('Controller Tests', function() {

    describe('Pedido Detail Controller', function() {
        var $scope, $rootScope;
        var MockEntity, MockPedido, MockVeiculo, MockItem_Servico;
        var createController;

        beforeEach(inject(function($injector) {
            $rootScope = $injector.get('$rootScope');
            $scope = $rootScope.$new();
            MockEntity = jasmine.createSpy('MockEntity');
            MockPedido = jasmine.createSpy('MockPedido');
            MockVeiculo = jasmine.createSpy('MockVeiculo');
            MockItem_Servico = jasmine.createSpy('MockItem_Servico');
            

            var locals = {
                '$scope': $scope,
                '$rootScope': $rootScope,
                'entity': MockEntity ,
                'Pedido': MockPedido,
                'Veiculo': MockVeiculo,
                'Item_Servico': MockItem_Servico
            };
            createController = function() {
                $injector.get('$controller')("PedidoDetailController", locals);
            };
        }));


        describe('Root Scope Listening', function() {
            it('Unregisters root scope listener upon scope destruction', function() {
                var eventType = 'autocenterMwmApp:pedidoUpdate';

                createController();
                expect($rootScope.$$listenerCount[eventType]).toEqual(1);

                $scope.$destroy();
                expect($rootScope.$$listenerCount[eventType]).toBeUndefined();
            });
        });
    });

});

/**
 * Data Transfer Objects used by Spring MVC REST controllers.
 */
package br.com.autocenter.web.rest.dto;

'use strict';

describe('Controller Tests', function() {

    describe('Cliente Detail Controller', function() {
        var $scope, $rootScope;
        var MockEntity, MockCliente, MockVeiculo;
        var createController;

        beforeEach(inject(function($injector) {
            $rootScope = $injector.get('$rootScope');
            $scope = $rootScope.$new();
            MockEntity = jasmine.createSpy('MockEntity');
            MockCliente = jasmine.createSpy('MockCliente');
            MockVeiculo = jasmine.createSpy('MockVeiculo');
            

            var locals = {
                '$scope': $scope,
                '$rootScope': $rootScope,
                'entity': MockEntity ,
                'Cliente': MockCliente,
                'Veiculo': MockVeiculo
            };
            createController = function() {
                $injector.get('$controller')("ClienteDetailController", locals);
            };
        }));


        describe('Root Scope Listening', function() {
            it('Unregisters root scope listener upon scope destruction', function() {
                var eventType = 'autocenterMwmApp:clienteUpdate';

                createController();
                expect($rootScope.$$listenerCount[eventType]).toEqual(1);

                $scope.$destroy();
                expect($rootScope.$$listenerCount[eventType]).toBeUndefined();
            });
        });
    });

});

'use strict';

describe('Controller Tests', function() {

    describe('Veiculo Detail Controller', function() {
        var $scope, $rootScope;
        var MockEntity, MockVeiculo, MockCliente, MockPedido;
        var createController;

        beforeEach(inject(function($injector) {
            $rootScope = $injector.get('$rootScope');
            $scope = $rootScope.$new();
            MockEntity = jasmine.createSpy('MockEntity');
            MockVeiculo = jasmine.createSpy('MockVeiculo');
            MockCliente = jasmine.createSpy('MockCliente');
            MockPedido = jasmine.createSpy('MockPedido');
            

            var locals = {
                '$scope': $scope,
                '$rootScope': $rootScope,
                'entity': MockEntity ,
                'Veiculo': MockVeiculo,
                'Cliente': MockCliente,
                'Pedido': MockPedido
            };
            createController = function() {
                $injector.get('$controller')("VeiculoDetailController", locals);
            };
        }));


        describe('Root Scope Listening', function() {
            it('Unregisters root scope listener upon scope destruction', function() {
                var eventType = 'autocenterMwmApp:veiculoUpdate';

                createController();
                expect($rootScope.$$listenerCount[eventType]).toEqual(1);

                $scope.$destroy();
                expect($rootScope.$$listenerCount[eventType]).toBeUndefined();
            });
        });
    });

});

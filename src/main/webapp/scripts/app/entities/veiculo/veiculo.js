'use strict';

angular.module('autocenterMwmApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('veiculo', {
                parent: 'entity',
                url: '/veiculos',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'autocenterMwmApp.veiculo.home.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/veiculo/veiculos.html',
                        controller: 'VeiculoController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('veiculo');
                        $translatePartialLoader.addPart('global');
                        return $translate.refresh();
                    }]
                }
            })
            .state('veiculo.detail', {
                parent: 'entity',
                url: '/veiculo/{id}',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'autocenterMwmApp.veiculo.detail.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/veiculo/veiculo-detail.html',
                        controller: 'VeiculoDetailController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('veiculo');
                        return $translate.refresh();
                    }],
                    entity: ['$stateParams', 'Veiculo', function($stateParams, Veiculo) {
                        return Veiculo.get({id : $stateParams.id});
                    }]
                }
            })
            .state('veiculo.new', {
                parent: 'veiculo',
                url: '/new',
                data: {
                    authorities: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'scripts/app/entities/veiculo/veiculo-dialog.html',
                        controller: 'VeiculoDialogController',
                        size: 'lg',
                        resolve: {
                            entity: function () {
                                return {
                                    placa: null,
                                    fabricante: null,
                                    modelo: null,
                                    ano_fabricacao: null,
                                    ano_modelo: null,
                                    cor: null,
                                    id: null
                                };
                            }
                        }
                    }).result.then(function(result) {
                        $state.go('veiculo', null, { reload: true });
                    }, function() {
                        $state.go('veiculo');
                    })
                }]
            })
            .state('veiculo.edit', {
                parent: 'veiculo',
                url: '/{id}/edit',
                data: {
                    authorities: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'scripts/app/entities/veiculo/veiculo-dialog.html',
                        controller: 'VeiculoDialogController',
                        size: 'lg',
                        resolve: {
                            entity: ['Veiculo', function(Veiculo) {
                                return Veiculo.get({id : $stateParams.id});
                            }]
                        }
                    }).result.then(function(result) {
                        $state.go('veiculo', null, { reload: true });
                    }, function() {
                        $state.go('^');
                    })
                }]
            })
            .state('veiculo.delete', {
                parent: 'veiculo',
                url: '/{id}/delete',
                data: {
                    authorities: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'scripts/app/entities/veiculo/veiculo-delete-dialog.html',
                        controller: 'VeiculoDeleteController',
                        size: 'md',
                        resolve: {
                            entity: ['Veiculo', function(Veiculo) {
                                return Veiculo.get({id : $stateParams.id});
                            }]
                        }
                    }).result.then(function(result) {
                        $state.go('veiculo', null, { reload: true });
                    }, function() {
                        $state.go('^');
                    })
                }]
            });
    });

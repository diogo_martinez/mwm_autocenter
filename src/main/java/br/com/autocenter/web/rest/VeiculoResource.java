package br.com.autocenter.web.rest;

import com.codahale.metrics.annotation.Timed;
import br.com.autocenter.domain.Veiculo;
import br.com.autocenter.repository.VeiculoRepository;
import br.com.autocenter.repository.search.VeiculoSearchRepository;
import br.com.autocenter.web.rest.util.HeaderUtil;
import br.com.autocenter.web.rest.util.PaginationUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing Veiculo.
 */
@RestController
@RequestMapping("/api")
public class VeiculoResource {

    private final Logger log = LoggerFactory.getLogger(VeiculoResource.class);
        
    @Inject
    private VeiculoRepository veiculoRepository;
    
    @Inject
    private VeiculoSearchRepository veiculoSearchRepository;
    
    /**
     * POST  /veiculos -> Create a new veiculo.
     */
    @RequestMapping(value = "/veiculos",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Veiculo> createVeiculo(@Valid @RequestBody Veiculo veiculo) throws URISyntaxException {
        log.debug("REST request to save Veiculo : {}", veiculo);
        if (veiculo.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("veiculo", "idexists", "A new veiculo cannot already have an ID")).body(null);
        }
        Veiculo result = veiculoRepository.save(veiculo);
        veiculoSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/veiculos/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("veiculo", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /veiculos -> Updates an existing veiculo.
     */
    @RequestMapping(value = "/veiculos",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Veiculo> updateVeiculo(@Valid @RequestBody Veiculo veiculo) throws URISyntaxException {
        log.debug("REST request to update Veiculo : {}", veiculo);
        if (veiculo.getId() == null) {
            return createVeiculo(veiculo);
        }
        Veiculo result = veiculoRepository.save(veiculo);
        veiculoSearchRepository.save(result);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("veiculo", veiculo.getId().toString()))
            .body(result);
    }

    /**
     * GET  /veiculos -> get all the veiculos.
     */
    @RequestMapping(value = "/veiculos",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<Veiculo>> getAllVeiculos(Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to get a page of Veiculos");
        Page<Veiculo> page = veiculoRepository.findAll(pageable); 
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/veiculos");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /veiculos/:id -> get the "id" veiculo.
     */
    @RequestMapping(value = "/veiculos/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Veiculo> getVeiculo(@PathVariable Long id) {
        log.debug("REST request to get Veiculo : {}", id);
        Veiculo veiculo = veiculoRepository.findOne(id);
        return Optional.ofNullable(veiculo)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /veiculos/:id -> delete the "id" veiculo.
     */
    @RequestMapping(value = "/veiculos/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteVeiculo(@PathVariable Long id) {
        log.debug("REST request to delete Veiculo : {}", id);
        veiculoRepository.delete(id);
        veiculoSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("veiculo", id.toString())).build();
    }

    /**
     * SEARCH  /_search/veiculos/:query -> search for the veiculo corresponding
     * to the query.
     */
    @RequestMapping(value = "/_search/veiculos/{query:.+}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<Veiculo> searchVeiculos(@PathVariable String query) {
        log.debug("REST request to search Veiculos for query {}", query);
        return StreamSupport
            .stream(veiculoSearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .collect(Collectors.toList());
    }
}

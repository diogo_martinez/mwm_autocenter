'use strict';

angular.module('autocenterMwmApp')
    .factory('Item_ServicoSearch', function ($resource) {
        return $resource('api/_search/item_Servicos/:query', {}, {
            'query': { method: 'GET', isArray: true}
        });
    });

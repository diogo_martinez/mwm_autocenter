'use strict';

angular.module('autocenterMwmApp')
    .factory('Pedido', function ($resource, DateUtils) {
        return $resource('api/pedidos/:id', {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    data = angular.fromJson(data);
                    data.data_pedido = DateUtils.convertLocaleDateFromServer(data.data_pedido);
                    return data;
                }
            },
            'update': {
                method: 'PUT',
                transformRequest: function (data) {
                    data.data_pedido = DateUtils.convertLocaleDateToServer(data.data_pedido);
                    return angular.toJson(data);
                }
            },
            'save': {
                method: 'POST',
                transformRequest: function (data) {
                    data.data_pedido = DateUtils.convertLocaleDateToServer(data.data_pedido);
                    return angular.toJson(data);
                }
            }
        });
    });

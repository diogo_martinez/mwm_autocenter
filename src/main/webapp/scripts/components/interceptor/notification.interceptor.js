 'use strict';

angular.module('autocenterMwmApp')
    .factory('notificationInterceptor', function ($q, AlertService) {
        return {
            response: function(response) {
                var alertKey = response.headers('X-autocenterMwmApp-alert');
                if (angular.isString(alertKey)) {
                    AlertService.success(alertKey, { param : response.headers('X-autocenterMwmApp-params')});
                }
                return response;
            }
        };
    });

package br.com.autocenter.repository.search;

import br.com.autocenter.domain.Item_Servico;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data ElasticSearch repository for the Item_Servico entity.
 */
public interface Item_ServicoSearchRepository extends ElasticsearchRepository<Item_Servico, Long> {
}

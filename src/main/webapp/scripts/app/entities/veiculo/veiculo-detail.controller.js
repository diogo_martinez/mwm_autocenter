'use strict';

angular.module('autocenterMwmApp')
    .controller('VeiculoDetailController', function ($scope, $rootScope, $stateParams, entity, Veiculo, Cliente, Pedido) {
        $scope.veiculo = entity;
        $scope.load = function (id) {
            Veiculo.get({id: id}, function(result) {
                $scope.veiculo = result;
            });
        };
        var unsubscribe = $rootScope.$on('autocenterMwmApp:veiculoUpdate', function(event, result) {
            $scope.veiculo = result;
        });
        $scope.$on('$destroy', unsubscribe);

    });

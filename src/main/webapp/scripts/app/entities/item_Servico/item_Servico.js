'use strict';

angular.module('autocenterMwmApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('item_Servico', {
                parent: 'entity',
                url: '/item_Servicos',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'autocenterMwmApp.item_Servico.home.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/item_Servico/item_Servicos.html',
                        controller: 'Item_ServicoController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('item_Servico');
                        $translatePartialLoader.addPart('global');
                        return $translate.refresh();
                    }]
                }
            })
            .state('item_Servico.detail', {
                parent: 'entity',
                url: '/item_Servico/{id}',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'autocenterMwmApp.item_Servico.detail.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/item_Servico/item_Servico-detail.html',
                        controller: 'Item_ServicoDetailController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('item_Servico');
                        return $translate.refresh();
                    }],
                    entity: ['$stateParams', 'Item_Servico', function($stateParams, Item_Servico) {
                        return Item_Servico.get({id : $stateParams.id});
                    }]
                }
            })
            .state('item_Servico.new', {
                parent: 'item_Servico',
                url: '/new',
                data: {
                    authorities: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'scripts/app/entities/item_Servico/item_Servico-dialog.html',
                        controller: 'Item_ServicoDialogController',
                        size: 'lg',
                        resolve: {
                            entity: function () {
                                return {
                                    descricao: null,
                                    periodo_manutencao: null,
                                    preco_unitario: null,
                                    id: null
                                };
                            }
                        }
                    }).result.then(function(result) {
                        $state.go('item_Servico', null, { reload: true });
                    }, function() {
                        $state.go('item_Servico');
                    })
                }]
            })
            .state('item_Servico.edit', {
                parent: 'item_Servico',
                url: '/{id}/edit',
                data: {
                    authorities: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'scripts/app/entities/item_Servico/item_Servico-dialog.html',
                        controller: 'Item_ServicoDialogController',
                        size: 'lg',
                        resolve: {
                            entity: ['Item_Servico', function(Item_Servico) {
                                return Item_Servico.get({id : $stateParams.id});
                            }]
                        }
                    }).result.then(function(result) {
                        $state.go('item_Servico', null, { reload: true });
                    }, function() {
                        $state.go('^');
                    })
                }]
            })
            .state('item_Servico.delete', {
                parent: 'item_Servico',
                url: '/{id}/delete',
                data: {
                    authorities: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'scripts/app/entities/item_Servico/item_Servico-delete-dialog.html',
                        controller: 'Item_ServicoDeleteController',
                        size: 'md',
                        resolve: {
                            entity: ['Item_Servico', function(Item_Servico) {
                                return Item_Servico.get({id : $stateParams.id});
                            }]
                        }
                    }).result.then(function(result) {
                        $state.go('item_Servico', null, { reload: true });
                    }, function() {
                        $state.go('^');
                    })
                }]
            });
    });

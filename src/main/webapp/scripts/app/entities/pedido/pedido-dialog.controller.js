'use strict';

angular.module('autocenterMwmApp').controller('PedidoDialogController',
    ['$scope', '$stateParams', '$uibModalInstance', 'entity', 'Pedido', 'Veiculo', 'Item_Servico',
        function($scope, $stateParams, $uibModalInstance, entity, Pedido, Veiculo, Item_Servico) {

        $scope.pedido = entity;
        $scope.veiculos = Veiculo.query();
        $scope.item_servicos = Item_Servico.query();
        $scope.load = function(id) {
            Pedido.get({id : id}, function(result) {
                $scope.pedido = result;
            });
        };

        var onSaveSuccess = function (result) {
            $scope.$emit('autocenterMwmApp:pedidoUpdate', result);
            $uibModalInstance.close(result);
            $scope.isSaving = false;
        };

        var onSaveError = function (result) {
            $scope.isSaving = false;
        };

        $scope.save = function () {
            $scope.isSaving = true;
            if ($scope.pedido.id != null) {
                Pedido.update($scope.pedido, onSaveSuccess, onSaveError);
            } else {
                Pedido.save($scope.pedido, onSaveSuccess, onSaveError);
            }
        };

        $scope.clear = function() {
            $uibModalInstance.dismiss('cancel');
        };
        $scope.datePickerForData_pedido = {};

        $scope.datePickerForData_pedido.status = {
            opened: false
        };

        $scope.datePickerForData_pedidoOpen = function($event) {
            $scope.datePickerForData_pedido.status.opened = true;
        };
}]);
